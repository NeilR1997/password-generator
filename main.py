#Password Generator Project
import random
letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
random.shuffle(letters)
numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
random.shuffle(numbers)
symbols = ['!', '#', '$', '%', '&', '(', ')', '*', '+']
random.shuffle(symbols)

print("Welcome to the PyPassword Generator!")
nr_letters= int(input("How many letters would you like in your password?\n")) 
nr_symbols = int(input("How many symbols would you like?\n"))
nr_numbers = int(input("How many numbers would you like?\n"))
amount_of_letters = 0
amount_of_numbers = 0
amount_of_symbols = 0
letters_password = ""
numbers_password = ""
symbols_password = ""

#Getting the Letters for the password
for letter in letters:
  if amount_of_letters < nr_letters:
    letters_password += letter
    amount_of_letters += 1

#Getting the numbers for the password

for number in numbers:
  if amount_of_numbers < nr_numbers:
    numbers_password += number
    amount_of_numbers += 1

#Getting the symbols for the password

for symbol in symbols:
  if amount_of_symbols < nr_symbols:
    symbols_password += symbol
    amount_of_symbols += 1

#Gets the string for the whole password

whole_password = letters_password + symbols_password + numbers_password

#How to randomise the order in which it appears
#1: Shuffle the lists that the letters, numbers and symbols are stored in
#2: After all the for loops are done getting the symbols, numbers, letters etc that the user requested add that all into one string
#3: Once that has done, turn it back into a list
#4: After that, use random.shuffle to shuffle that list like how we shuffled the lists at the start
#5: Then use the join command on the variable we stored it all in using the "" to specify it is a string and instead the variable we stored the list in to do it
#After all this we will have a random password every time based on the inputs the user provided for it

#Randomise the password

random_password = list(whole_password)
random.shuffle(random_password)
final_password = "".join(random_password)
print(final_password)


#Eazy Level - Order not randomised:
#e.g. 4 letter, 2 symbol, 2 number = JduE&!91


#Hard Level - Order of characters randomised:
#e.g. 4 letter, 2 symbol, 2 number = g^2jk8&P